import './bootstrap';

import { createApp } from 'vue/dist/vue.esm-bundler';
import Xzone from './components/xzone.vue';

const app = createApp({
	components: {
		Xzone,
	}
});

app.mount('#app');
