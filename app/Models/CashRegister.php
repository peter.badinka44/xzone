<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CashRegister extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'cislo_pokladny',
        'cislo_predajny',
        'celkova_castka',
        'stranou_do_banky',
        'rezerva',
        'bankovky',
    ];

    protected $casts = [
        'bankovky' => 'array'
    ];
}
